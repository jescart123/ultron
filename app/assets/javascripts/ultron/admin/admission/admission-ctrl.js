( function() {
  'use strict';

  angular.module("admin.ctrl.admissionCtrl", [])
  .controller('admisionController', ['$scope', '$filter', 'User', 'Project', 'ProjectOnUser', 'PermittedProject',
  	function($scope, $filter, User, Project, ProjectOnUser,PermittedProject){
      $scope.users = [];
      $scope.projects = [];
      $scope.selection=[];
      $scope.projectOnUser = {};

      User.query({'config': 'false'}).$promise.then(function(data) {
        $scope.users = data;
      });

      $scope.$watch('testUser', function(user) {
        if(user != null){
          $scope.user_id = user._id.$oid;
          Project.query({ user_id: $scope.user_id }).$promise.then(function(data) {
            $scope.projects = data;
          });
        }
      });
      

      $scope.accept = function(user_id) { 
        var confirmed = true;
        User.update({ id: user_id }, { confirmed: confirmed }).$promise.then(function(data) {
          alert("Accepted!");
          User.query().$promise.then(function(data) {
            $scope.users = data;
          });
        });
      }

      $scope.decline = function(user_id) { 
        var declined = true;
        User.update({ id: user_id }, { declined: declined }).$promise.then(function(data) {
          alert("Declined!");
          User.query().$promise.then(function(data) {
            $scope.users = data;
          });
        });
      }

      $scope.admissions = function() { 
        User.query({'config': 'false'}).$promise.then(function(data) {
          $scope.users = data;
        });
      }

      $scope.permissions = function() { 
        PermittedProject.query().$promise.then(function(data) {
          $scope.users = data;
        });
      }

      $scope.setPermission = function(project_id, user_id) {
        console.info('project_id ->',project_id);
        console.info('user_id ->',user_id);
        $scope.projectOnUser.project_id = project_id;
        $scope.projectOnUser.user_id = user_id;
        var newProjectOnUser = new ProjectOnUser($scope.projectOnUser);
        newProjectOnUser.$save(newProjectOnUser, function(data) {
        });
      }

      $scope.permittedProject = function(permitted_projects, project_id) {
        var assigned_project = $filter('filter')(permitted_projects, { project_id: project_id });
        if(assigned_project.length > 0) {
          return true;
        } else {
          return false;
        }
      }
  	}
  ]);
})(); 
