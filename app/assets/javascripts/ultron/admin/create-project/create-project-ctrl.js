
( function() {
  'use strict';

  angular.module("admin.ctrl.createProjectCtrl", [])
  .controller('createProjectController', ['$scope', '$location', 'Project',
  	function($scope, $location, Project) {
      $scope.project = {};

      $scope.$watch('testUser', function(user) {
        if(user != null){
          $scope.user_id = user._id.$oid;
        }
      });
      
      $scope.createProject = function() {
        if($scope.project.title && $scope.project.description) {
          if(($scope.project.rails || $scope.project.angularJS )&&($scope.project.mongoDB || $scope.project.postgreSQL || $scope.project.mySQL)&&($scope.project.heroku || $scope.project.windowsAzure || $scope.project.eC2)){
            $scope.project.user_id = $scope.user_id;
            var newProject = new Project($scope.project);
            newProject.$save(newProject, function(data) {
              $location.path('/project/'+data.id+'/tutorial/add');
            });
          }
        }
    	}
    }
  ]);
})(); 
