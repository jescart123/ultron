( function() {
  'use strict';

  angular.module("admin.ctrl.projectListCtrl", [])
  .controller('projectListController', ['$scope', 'Project', 'ProjectOnUser',
  	function($scope, Project, ProjectOnUser) {
      $scope.projects = [];
      $scope.$watch('testUser', function(user) {
        if(user != null){
          if(user.user_type == 1){
            Project.query({ user_id: user._id.$oid }).$promise.then(function(data) {
              $scope.projects = data;
            });  
          }else{
            ProjectOnUser.query({ user_id: user._id.$oid }).$promise.then(function(data) {
              $scope.projects = data;
            }); 
          }   
        }
      }); 
  	}
  ]);
})(); 
