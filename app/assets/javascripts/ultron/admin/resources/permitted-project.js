(function() {
  'use strict';

  angular.module('admin.resource.permittedProject', ['ngResource'])
  .factory("PermittedProject", [
    '$resource', function($resource) {
      return $resource("/api/admin/permitted_projects/:id.json", {
        id: "@id"
      },{
        update: { method: 'PUT' }
      });
  }]);
})();