(function() {
  'use strict';

  angular.module('admin.resource.projectOnUser', ['ngResource'])
  .factory("ProjectOnUser", [
    '$resource', function($resource) {
      return $resource("/api/admin/project_on_users/:id.json", {
        id: "@id"
      },{
        update: { method: 'PUT' }
      });
  }]);
})();