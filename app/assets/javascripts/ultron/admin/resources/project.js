(function() {
  'use strict';

  angular.module('admin.resource.project', ['ngResource'])
  .factory("Project", [
    '$resource', function($resource) {
      return $resource("/api/admin/projects/:id.json", {
        id: "@id"
      },{
        update: { method: 'PUT' }
      });
  }]);
})();