(function() {
  'use strict';

  angular.module('admin.resource.step', ['ngResource'])
  .factory("Step", [
    '$resource', function($resource) {
      return $resource("/api/admin/steps/:id.json", {
        id: "@id"
      },{
        update: { method: 'PUT' }
      });
  }]);
})();