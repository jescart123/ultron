(function() {
  'use strict';

  angular.module('admin.resource.user', ['ngResource'])
  .factory("User", [
    '$resource', function($resource) {
      return $resource("/api/admin/users/:id.json", {
        id: "@id"
      },{
        update: { method: 'PUT' }
      });
  }]);
})();