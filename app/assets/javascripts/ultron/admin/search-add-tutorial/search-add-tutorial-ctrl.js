( function() {
  'use strict';

  angular.module("admin.ctrl.searchAddTutorial", [])
  .controller('searchAddTutorialController', [ '$scope', '$location', '$routeParams', 'Project', 'Tutorial', 'TutorialOnProject',
    function($scope, $location, $routeParams, Project, Tutorial, TutorialOnProject) {
      $scope.project = {};
      $scope.tutorials = [];
      $scope.selection=[];
      $scope.tutorialOnProject = {};
      $scope.max = 5;
      
      Project.get({ id: $routeParams.project_id }).$promise.then(function(data) {
       $scope.project = data;
      });

      Tutorial.query().$promise.then(function(data) {
       $scope.tutorials = data;
      });

      $scope.toggleSelection = function(tutorial_id) {
        var idx = $scope.selection.indexOf(tutorial_id);
        if (idx > -1) {
          $scope.selection.splice(idx, 1);
        }
        else {
          $scope.selection.push(tutorial_id);
        }
      };

      $scope.searchAddTutorial = function() {
        angular.forEach($scope.selection,function(value,index){
          $scope.tutorialOnProject.project_id = $routeParams.project_id;
          $scope.tutorialOnProject.tutorial_id = value;
          var newSearchAddTutorial = new TutorialOnProject($scope.tutorialOnProject);
          newSearchAddTutorial.$save(newSearchAddTutorial, function(data) {
          });
        });
        alert('Successfully Added!');
        $location.path('/project/'+ $routeParams.project_id+'/tutorial/add');
      };
    }
  ]);
})(); 
