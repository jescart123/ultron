( function() {
  'use strict';

  angular.module('admin.ctrl.sideBar', [])
  .controller('sideBarController', ['Auth', '$scope', '$location', 
    function(Auth, $scope, $location) {
      Auth.currentUser().then(function(user) {
        $scope.testUser = user;
      }); 
    }
  ]);

})();