( function() {
  'use strict';

  angular.module("admin.ctrl.viewStepCtrl", ['angularFileUpload'])
  .controller('viewStepController', [ '$scope', '$routeParams', 'Project', 'Tutorial', 'Step', 'FileUploader', 'Image', 'cfpLoadingBar',
  	function($scope,$routeParams, Project, Tutorial, Step, FileUploader, Image, cfpLoadingBar ) {
      $scope.steps =[];
      $scope.step ={};
      $scope.tutorial ={};
      $scope.viewStepDetails = "1";
      $scope.edit = false;
      $scope.max = 5;
      $scope.project = {};
      $scope.image = null;
      $scope.images = [];
      $scope.uploading = false;
      $scope.asd = false;


      Project.get({ id: $routeParams.project_id }).$promise.then(function(data) {
       $scope.project = data;
      });
      
      Tutorial.get({ id: $routeParams.tutorial_id }).$promise.then(function(data) {
       $scope.tutorial = data;
      });

      Step.query({ tutorial_id: $routeParams.tutorial_id }).$promise.then(function(data) {
       $scope.steps = data;
      });

      Image.query({ tutorial_id: $routeParams.tutorial_id }).$promise.then(function(data) {
       $scope.images = data;
      });

      $scope.stepDetails = function(step_id) { 
        $scope.viewStepDetails = "2";
        Step.get({ id: step_id }).$promise.then(function(data) {
          $scope.step = data;
        });

      }

      $(document).ready(function() {
        $('#tooltip_1').tooltip();
      });

      $scope.editForm = function() { 
        $scope.edit = true;
      }

      $scope.cancelEdit = function() { 
        $scope.edit = false;
        Tutorial.get({ id: $routeParams.tutorial_id }).$promise.then(function(data) {
         $scope.tutorial = data;
        });
      }

      $scope.editTutorial = function() { 
        Tutorial.update({ id: $routeParams.tutorial_id}, $scope.tutorial ).$promise.then(function(data) {
          $scope.edit = false;
        });
      }

      $scope.addStep = function() { 
        $scope.viewStepDetails = "3";
        $scope.step.id = "";
        $scope.step.title = "";
        $scope.step.body = "";
        $scope.step.tutorial_id = "";
      }

      var tutorial_id = $routeParams.tutorial_id;
      var uploader = $scope.uploader = new FileUploader({ url: '/api/admin/images/upload_file_image'});
      uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.uploading = false;
        alert("Successfully uploaded image!");
        $scope.image = response.image_url;
        Image.update({ id: response.id, tutorial_id: tutorial_id }).$promise.then(function(data) {
          cfpLoadingBar.complete();
          $scope.asd = false;
          Image.query({ tutorial_id: $routeParams.tutorial_id }).$promise.then(function(data) {
            $scope.images = data;
          });
        });
      };

      uploader.onProgressItem =  function(fileItem, progress) {
        $scope.uploading = true;
      };

      $scope.imageProfile = function() {
        $scope.asd = true;
        cfpLoadingBar.start();
        uploader.uploadAll();
      }

      $scope.addNewStep = function() { 
        if($scope.step.title && $scope.step.body){
          $scope.step.tutorial_id = $routeParams.tutorial_id;
          var newStep= new Step($scope.step);
          newStep.$save(newStep, function(data) {
            Step.query({ tutorial_id: $routeParams.tutorial_id }).$promise.then(function(data) {
             $scope.steps = data;
             $scope.viewStepDetails = "1";
            });
          }); 
        }
      }

      $scope.editStep= function() { 
        $scope.viewStepDetails = "4";
      }

      $scope.saveEditedStep = function(step_id) { 
        Step.update({ id: step_id}, $scope.step ).$promise.then(function(data) {
          Step.query({ tutorial_id: $routeParams.tutorial_id }).$promise.then(function(data) {
           $scope.steps = data;
          });
          $scope.viewStepDetails = "2";
          Step.get({ id: step_id }).$promise.then(function(data) {
            $scope.step = data;
          });
        });
      }

  	}
  ]);
})(); 
