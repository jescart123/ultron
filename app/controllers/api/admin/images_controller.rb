class API::Admin::ImagesController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: [:upload_file_image]

  def upload_file_image
      @image = Image.create({image_file: params[:file] }) 
      Rails.logger.info params.inspect
  end

  def index
  	@images = Image.where({ tutorial_id: params["tutorial_id"] })
 	end

	def update
  	@update = Image.where({ id: params["id"], }).first
    if params["tutorial_id"] != nil
 		  @update.update_attributes({ tutorial_id: params["tutorial_id"] })
    elsif params["project_id"] != nil
      @update.update_attributes({ project_id: params["project_id"] })
    end
  end


end