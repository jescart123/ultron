class API::Admin::ProjectOnUsersController < ApplicationController
  before_filter :authenticate_user!

  def create
  	project = ProjectOnUser.where({ project_id: projectOnUser_params['project_id'], user_id: projectOnUser_params['user_id'] }).first
    if project
    	project.delete
    else
	    @projectOnUser = ProjectOnUser.create(projectOnUser_params)
	  end
  end

  def index
    @projects = ProjectOnUser.where({ user_id: params["user_id"] })
  end


  private
    def projectOnUser_params
      params.require(:project_on_user).permit(:project_id, :user_id)
    end
  
end
