class API::Admin::StepsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @step = Step.create(step_params)
  end

  def show
    @step = Step.where({ id: params[:id] }).first  
  end

  def index
    @steps = Step.where({ tutorial_id: params["tutorial_id"] }).order("created_at ASC")
  end

  def destroy
    @step = Step.where({ id: params[:id] }).first  
    @step.destroy
  end

  def update
    @update = Step.where({ id: params["id"] }).first
    @update.update_attributes(step_params)
  end

  private
    def step_params
      params.require(:step).permit(:title, :body ,:tutorial_id)
    end
  
end
