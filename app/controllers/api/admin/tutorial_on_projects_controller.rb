class API::Admin::TutorialOnProjectsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @tutorialOnProject = TutorialOnProject.create(tutorialOnProject_params)
  end

  def show
    @tutorialOnProject = TutorialOnProject.where({ id: params[:id] }).first  
  end

  def destroy
    @tutorialOnProject = TutorialOnProject.where({ id: params[:id] }).first  
    @tutorialOnProject.destroy
  end

  def index
    @tutorialOnProjects = TutorialOnProject.where({ project_id: params["project_id"] })
  end

  private
    def tutorialOnProject_params
      params.require(:tutorial_on_project).permit(:project_id, :tutorial_id)
    end
  
end
