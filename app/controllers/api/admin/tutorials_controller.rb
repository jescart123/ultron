class API::Admin::TutorialsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @tutorial = Tutorial.create(tutorial_params)
  end

  def show
    @tutorial = Tutorial.where({ id: params[:id] }).first  
  end

  def index
    @tutorials = Tutorial.all
  end

  def update
    @update = Tutorial.where({ id: params["id"] }).first
    @update.update_attributes(tutorial_params)
  end


  private
    def tutorial_params
      params.require(:tutorial).permit(:title, :description, :implementedOn, :difficulty)
    end
  
end
