class Project
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title
  field :description
  field :rails,              type: Boolean, default: false
  field :angularJS,          type: Boolean, default: false
  field :mongoDB,            type: Boolean, default: false
  field :postgreSQL,         type: Boolean, default: false
  field :mySQL,              type: Boolean, default: false
  field :heroku,             type: Boolean, default: false
  field :windowsAzure,       type: Boolean, default: false
  field :eC2,                type: Boolean, default: false

  has_many :tutorial_on_projects
  has_many :project_on_users
  belongs_to :user
  
end