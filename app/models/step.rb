class Step
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title
  field :body
  
	belongs_to :tutorial
  
end