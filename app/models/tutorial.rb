class Tutorial
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title
  field :description
  field :implementedOn
  field :difficulty,    	 type: Integer

  has_many :tutorial_on_projects
  has_many :steps
  has_many :images
  
end