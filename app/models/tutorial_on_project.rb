class TutorialOnProject
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :project
  belongs_to :tutorial
  
end