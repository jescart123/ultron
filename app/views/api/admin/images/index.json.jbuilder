json.array! @images do |image|
	json.image_file image.image_file
  json.image_url image.image_file.main.url
end
