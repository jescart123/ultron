if @image.valid?
  json.image_url @image.image_file.main.url
else
  json.success false
end