json.array! @steps do |step|
  json.id step.id.to_s
  json.title step.title
  json.body step.body[0..100]
  json.tutorial_id step.tutorial_id.to_s
end
