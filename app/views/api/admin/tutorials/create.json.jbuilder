if @tutorial.valid?
  json.success true
  json.id @tutorial.id.to_s
  json.title @tutorial.title
  json.description @tutorial.description
  json.implementedOn @tutorial.implementedOn
  json.difficulty @tutorial.difficulty
else
  json.success false
end