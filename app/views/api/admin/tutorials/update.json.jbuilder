  json.success true
  json.id @update.id.to_s
  json.title @update.title
  json.description @update.description
  json.implementedOn @update.implementedOn
  json.difficulty @update.difficulty