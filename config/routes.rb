Rails.application.routes.draw do
  devise_for :users
  resources :home, only: [:index]
  root 'home#index'  

  namespace :api, defaults: { format: :json } do
    namespace :admin do
      resources :projects, only: [:create, :show, :index, :update]
      resources :tutorials, only: [:create, :index, :show, :update]
      resources :tutorial_on_projects, only: [:create, :index, :destroy]
      resources :steps, only: [:create, :index, :show, :destroy, :update]
      resources :users, only: [:index, :update]
      resources :project_on_users, only: [:create, :index]
      resources :permitted_projects, only: [:index]
      resources :images, except: [:edit] do
        collection do
          post 'upload_file_image'
        end
      end

  	end
	end
end
