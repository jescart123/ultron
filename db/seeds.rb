# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create({ email: 'qwe@qwe.com', password: 'asdfasdfasdf', confirmed: 'true', name:'asd', user_type: '1'})
User.create({ email: 'juan@juan.com', password: 'asdfasdfasdf', name:'juan'})
User.create({ email: 'john@john.com', password: 'john@john.com', name:'john'})


#(0..5).each do
#  user = User.first
#  project = Project.create ({ 
#  	:user_id => user.id,
#  	:title => Faker::Lorem.word,
#	  :description => Faker::Lorem.paragraph,
#	  :rails => true,
#	  :angularJS => true,
#	  :mongoDB => true,
#	  :heroku => true,
#  })
#  (0..5).each do
#	  tutorial = Tutorial.create ({ 
#		  :title => Faker::Lorem.word,
#		  :description => Faker::Lorem.paragraph,
#		  :implementedOn => "Rails",
#		  :difficulty => 3,
#	  })
#	  tutorialOnProject = TutorialOnProject.create ({ 
# 		:project_id => project.id,
#	    :tutorial_id => tutorial.id,
#	  })
#	end
	
#end

#(0..10).each do
#  project = Project.all.sample
#  pfunction = Pfunction.create ({ 
#  	:project_id => project.id,
#  	:url => Faker::Internet.url,
#	  :title => Faker::Lorem.word,
#	  :description => Faker::Lorem.paragraph,
#	  :implementedOn => "Rails",
#	  :difficulty => 3,
#  })
#end
#(0..10).each do
#  project = Project.all.sample
#  pfunction = Pfunction.all.sample
#  functionOnProject = FunctionOnProject.create ({ 
#  	:project_id => project.id,
#    :pfunction_id => pfunction.id,
#  })
#end
